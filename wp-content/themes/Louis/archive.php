<?php
/**
 * The template for displaying archive pages.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package louis
 */

get_header('archives');
$cat = get_category(get_query_var('cat'))->cat_ID;

?>
<div id="blogposts">

<div class="wrapper">

<div class="row">
<?php

$rooms = new WP_Query([
    'cat' => $cat,
    'post_type' => 'room',
    'posts_per_page' => '9'
]);

if ($rooms->have_posts()) :
    while ( $rooms->have_posts() ) : $rooms->the_post(); ?>
        <?php get_template_part( 'inc/partials/content', '' ); ?>
    <?php endwhile; ?>
    <?php wp_reset_postdata(); ?>
<?php endif; ?>

</div><!-- end row -->

 
 
<?php louis_pagination(); ?>

</div><!-- End Wrapper -->
</div><!-- End Wrapper -->


</div><!-- End blogposts -->



<?php get_footer();